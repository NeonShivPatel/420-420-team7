# 420-420-team7

Django Project for Winter 2021
Made By: Shiv Patel & Edris Zoghlami 

How to Run the Program: 
IN settings.py. Change the database information to personal choice in order to access the database.
To run the program simply go onto your command line and go into the pozozo folder containing the manage.py file.
Then run the command "python manage.py runserver"
Following that go on any web browser and type in "localhost:8000" or "127.0.0.1:8000"
Now you should have a website you can navigate

In order to use the Application to its fullest, create a superuser by signing up. After this, log in to use the application. You can  buy and sell items when
logged in. When viewing the profile, you can see the full purchase history with information such as the item name, description and item price

To access the page, one needs to have the program running and then type in "/admin" onto the "localhost:8000" or "127.0.0.1:8000"
Following that action, one must input user and password to successfully enter the admin page


Disclamer: we had lots of issues with the cross application urls so in order to have a working project we decided to merge the web app and item app.