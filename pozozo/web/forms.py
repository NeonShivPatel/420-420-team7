# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from web.models import Item







#the form for when we want to sell an item

class SellItemForm(ModelForm):
    class Meta:
        model = Item
        fields = ['item_owner','item_name', 'item_description', 'item_price']
        
        
#the form for when we want to buy an item
class BuyItemForm(forms.Form):
    
    item_id = forms.IntegerField(label='item_id')