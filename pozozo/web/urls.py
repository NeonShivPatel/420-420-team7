# -*- coding: utf-8 -*-

from django.contrib import admin
from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.urls import include


#url patterns for each website page that we have, login and logout pages have the django
#created views to make things easier
urlpatterns = [
    path('', views.home, name="web-home"),
    path('shop', views.shop, name="web-shop"),
    path('login', auth_views.LoginView.as_view(template_name="web/login.html"), name="web-login"),
    path('logout', auth_views.LogoutView.as_view(template_name="web/logout.html"), name="web-logout"),
    path('register', views.register, name="web-register"),
    path('profile/', views.profile, name="web-profile"),
    path('sell',views.sell_item),
    path('display/', views.sell_item),
    path('buy',views.buy_item,name="buy_item"),
]
