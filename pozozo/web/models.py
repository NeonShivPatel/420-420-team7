from django.db import models
from django.contrib.auth.models import User

# Create your models here.

#Class Profile in charge of user profile and data
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to="profile_pics")
    
    def __str__(self):
        return f'{self.user.username} Profile'
    
    
    
    
#creating Item for when we sell an item and add it to the database    
class Item(models.Model):
    
    item_owner = models.CharField(max_length=50)
    item_name = models.CharField(max_length=50)
    item_description = models.CharField(max_length=100) 
    item_price = models.IntegerField(default=0)
    
    def __str__(self):
        return self.item_name
    
class Meta:
    managed = True
    db_table = 'web_item'
    
    
    
#creating bought item for when we buy item and send it to the database    
class BoughtItem(models.Model):
    
    item_owner = models.CharField(max_length=50)
    item_name = models.CharField(max_length=50)
    item_description = models.CharField(max_length=100) 
    item_price = models.IntegerField(default=0)
    
    def __str__(self):
        return self.item_name
    