from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from django.contrib.auth.decorators import login_required


from django.shortcuts import render
from item.models import Item
from django.http import HttpResponseRedirect
from django.urls import reverse
from web.models import Item , BoughtItem
from .forms import SellItemForm 
from .forms import BuyItemForm 
from django.db import models
from django.template import loader
from django.http import HttpResponse
from django.forms import modelformset_factory


    
# Create your views here.

#Method in charge of rendering the home page
def home(request):
    return render(request, 'web/index.html')

#Method in charge of rendering the shop page
def shop(request):
   sellItem = Item.objects.all()

   return render(request, 'web/shop.html', {'sellItem': sellItem})
#Method in charge of rendering the sell page

#Method in charge of rendering the login page
def login(request):
    return render(request, 'web/login.html')

#Method in charge of rendering the registration page
def register(request):
    #If statement for when customer submits a form to create a user
    if request.method == "POST":
        form = UserCreationForm(request.POST)

        form.save()
        if form.is_valid():
            messages.success(request, f'Account created')

        #if statement for when the information in the form is valid
        if form.is_valid():
            form.save()
            #A valid form will redirect the customer to the login page

            return redirect("web-login")
    else:
        form = UserCreationForm()
    return render(request, 'web/registration.html',{'form': form})


#method(view) to sell item, it takes form data and converts it to object to add
#to the Item table.
@login_required
def sell_item(request):
    
    if request.method == 'POST':
        form = SellItemForm(request.POST)
       
        if form.is_valid():
            print(form)
            u = form.save()
            sellItem = Item.objects.all()

            return render(request, 'web/shop.html', {'sellItem': sellItem})
        
             
            
    else:
        
        form_class = SellItemForm

    
    return render(request, 'web/sell.html', {
     
        
    })






#Method in charge of rendering the profile page, only accessible if logged in
@login_required
def profile(request):
    
    boughtItems = BoughtItem.objects.filter(item_owner= request.user.username)
    
    return render(request, 'web/profile.html', {'boughtItems': boughtItems})


@login_required

#method(view) for buying a specific item. It removes the item from the buy page, 
#changes the owner to the person that bought the item and adds the item to 
#boughtItems table
def buy_item(request):

    form = BuyItemForm(request.POST)
   
    if form.is_valid():
        
        boughtItem = Item.objects.filter(id= form.cleaned_data['item_id'])
        Item.objects.filter(id= form.cleaned_data['item_id']).update(item_owner= request.user.username)
        for row in boughtItem.values():
            BoughtItem.objects.create(**row)
            
        boughtItem.delete()
            
        sellItem = Item.objects.all()
        return render(request, 'web/shop.html', {'sellItem': sellItem})
        
    
    
    
    
    
    
    
    
    
    




