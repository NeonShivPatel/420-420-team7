# -*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm
from item.models import Item


class SellItemForm(ModelForm):
    class Meta:
        model = Item
        fields = ['item_name', 'item_description', 'item_price']