from django.db import models

# Create your models here.
    
    
    
class Item(models.Model):
    
    item_name = models.CharField(max_length=50)
    item_description = models.CharField(max_length=100) 
    item_price = models.IntegerField(default=0)
    
    def __str__(self):
        return self.item_name
    
    
